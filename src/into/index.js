/**
 * Into function (see {@link intointo into}).
 * Also exposes {@link intoaddInjestMethod addInjestMethod} and {@link intoinjest injest}.
 *
 * The {@link intoaddInjestMethod addInjestMethod} function can be used to add injestion capabilities to any data structure.
 * 
 * The {@link intoinjest injest} function is used internaly in into, but can be used separatly to 'consume' values
 *
 * @namespace into
 */

const transduce = require("../transducers/transduce.js");
const reduce = require("../reduce");

const {
    completing
} = require("../util.js");

const INJEST_METHOD = "@@com.boogie666.tranducers.util/injest_" + Math.random() + "@@";

function injest_method(o) {
    return o[INJEST_METHOD];
}

function can_injest(o) {
    return !!injest_method(o);
}


/**
 * Protocol used for injesting
 * There are not public methods to this protocol. 
 * The only way to extend a class to use this protocol is to use the  {@link intoaddInjestMethod addInjestMethod}
 *
 * @memberof into
 */
function Injesting() {}


/**
 * Polymorphic function that takes a injesting data structure and a item and incorporates it into the data structure
 *
 * @memberof into
 * @example
 * require("js-transducers/array"); //this makes arrays {Injesting} by using array.push
 *
 * injest([], 1); // returns [1]
 *
 * @param {into~Injesting} xs a data structure that can injest values
 * @param {any} item an item to be injested into the data structure
 */
function injest(xs, item) {
    if (can_injest(xs)) {
        return injest_method(xs).call(xs, item);
    }
    throw xs + " can't injest " + item;
}

/**
 * Monkey patch helper function. Use to make any data structure {@link intoInjesting Injesting}.
 *
 * @memberof into
 * @example
 * require("js-transducers/into"); 
 *
 * //for native js arrays this is the implementation used
 *
 * into.addInjestMethod(Array.prototype, function(item){
 *  this.push(item);
 *  return this;
 * });
 *
 * @param {Object} proto the prototype of any class that will become {@link intoInjesting Injesting} after this call
 * @param {function} fn the method implementation that will be attached to the prototype
 */
function addInjestMethod(proto, fn) {
    proto[INJEST_METHOD] = fn;
}


function intoWith_3(to, withFn, from) {
    return reduce(withFn, to, from);
}

function intoWith_4(to, xf, withFn, from) {
    return transduce(xf, completing(function(result, item) {
        return withFn(result, item);
    }), to, from);
}

/**
 *
 * Inserts values from the 'from' reducible in the 'to' value using the 'withFn' function
 *
 * @memberof into
 * @param {into.Injesting} to any data structure that can {@link intoinjest inject}
 * @param {Transducer} [xform=map(identity)] a transducer
 * @param {function} withFn - a reducing function
 * @param {reduce.Reducible} from any data structure that can {@link reduceselfReduce selfReduce}
 * 
 */
function intoWith(to, xf, withFn, from) {
    switch (arguments.length) {
        case 3:
            return intoWith_3(to, xf, withFn);
        default:
            return intoWith_4(to, xf, withFn, from);
    }
}


function into_3(to, xform, from) {
    return intoWith_4(to, xform, injest, from);
}

function into_2(to, from) {
    return intoWith_3(to, injest, from);
}

/**
 * 
 * Insert values from the 'from' reducible to the 'to' injestable
 *
 * @memberof into
 * @param {into.Injesting} to any data structure that can {@link intoinjest injest} values
 * @param {Transducer} [xform=map(identity)] a transducer
 * @param {reduce.Reducible} from any data structure that can {@link reduceselfReduce selfReduce}
 */
function into(to, xform, from) {
    switch (arguments.length) {
        case 2:
            return into_2(to, xform);
        default:
            return into_3(to, xform, from);
    }
}

into.injest = injest;
into.addInjestMethod = addInjestMethod;
into.into = into;
into.intoWith = intoWith;
module.exports = into;
