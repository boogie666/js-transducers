const { isReduced, addSelfReduceMethod } = require("./reduce");

function Value(val) {
    this.val = val;
}

function pull(process, reducible, counter) {
    var result = [];
    if (counter >= reducible.length) {
        return process(result);
    }
    while (result.length === 0) {
        if (counter === -1 || counter >= reducible.length) {
            process(result);
            return counter;
        }
        result = process(result, reducible[counter]);
        if (isReduced(result)) {
            process(result.value);
            return -1;
        }
        counter = counter + 1;
    }
    return counter;
}

function LazySeq(xform, state, state_counter, reducible, coll_counter) {
    this.first = function() {
        if (state[state_counter]) {
            return state[state_counter].val;
        }
        if (coll_counter === -1 || coll_counter >= reducible.length) {
            return undefined;
        }
        coll_counter = pull(xform, reducible, coll_counter);
        return state[state_counter].val;
    };

    this.rest = function() {
        this.first();
        return new LazySeq(xform, state, state_counter + 1, reducible, coll_counter);
    };
}

addSelfReduceMethod(LazySeq.prototype, function(f, init){
    if (isReduced(init)) {
        return init.value;
    }
    var list = this;
    var item = list.first();
    while(item !== undefined){
        init = f(init, item);
        if(isReduced(init)){
            return init.value;
        }
        list = list.rest();
        item = list.first();
    }
    return init;

});

function sequence(xform, reducible) {
    var state = [];
    var process = xform(function(result, item) {
        switch (arguments.length) {
        case 1:
            return result;
        default:
            state.push(new Value(item));
            return result;
        }
    });

    return new LazySeq(process, state, 0, reducible, 0);
}


module.exports = sequence;
