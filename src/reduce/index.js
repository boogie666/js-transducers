/**
 * A better version of the reduce  function that knows how to stop under certian conditions.
 *
 * exposes {@link reducereduce reduce} 
 *         {@link reducereduced reduced} 
 *         {@link reduceisReduced isReduced}
 *         {@link reduceunreduced unreduced} 
 *         {@link reduceaddSelfReduceMethod addSelfReduceMethod} 
 *
 * @namespace reduce
 */

/**
 * Maker object to stop reduce.
 *
 * @memberof reduce
 */
function Reduced(val) {
    this.value = val;
}

/**
 * Protocol. The only way add this protocol to a class is via the {@link reduceaddSelfReduceMethod addSelfReduceMethod}.
 * @memberof reduce
 */
function Reducible(){}


/**
 * Marks a value as reduced. 
 *
 * This stops reduction.
 * @memberof reduce
 * @param {any} x anything
 * @returns {reduce.Reduced} the value wraped up in a Reduced value.
 */
function reduced(x) {
    return new Reduced(x);
}

/**
 * Returns true if x is Reduced
 * @memberof reduce
 * @param {any} x a possibly reduced value
 * @returns {boolean} true if x is Reduced, false otherwise
 */
function isReduced(x) {
    return x instanceof Reduced;
}

/**
 * Returns a unreduced value. If possible_reduced is reduced the it gets unwrapped (only once) otherwise it just gets returned.
 *
 * @memberof reduce
 * @param {any|reduce.Reduced} possible_reduced a possibly reduced value
 * @returns {any} unwrapped reduced value
 */
function unreduced(possible_reduced) {
    if (isReduced(possible_reduced)) {
        return possible_reduced.value;
    }
    return possible_reduced;
}




const SELF_REDUCE_METHOD = "@@com.boogie666.transducers/self_reduce_" + Math.random() + "@@";

function reduce_method(o) {
    return o[SELF_REDUCE_METHOD];
}

function can_self_reduce(xs) {
    return !!reduce_method(xs);
}
/**
 * Extends given prototype  to add the "selfReduce" method.
 *
 * @example
 *  // this is the way native js arrays are reduced.
 *  addSelfReduceMethod(Array.prototype, function(f, init){
 *    if(isReduced(init)){
 *      return unreduced(init);
 *    }
 *    for(var i = 0; i < this.length; i++){
 *      init = f(init, this[i]);
 *      if(isReduced(init)){
 *        return unreduced(init);
 *      }
 *    }
 *    return init;
 *  });
 *
 * @memberof reduce
 * @param {Object} proto a prototype of a class (i.e. Array.prototype)
 * @param {function} fn the method to be used for "self reduction"
 *
 */
function addSelfReduceMethod(proto, fn) {
    proto[SELF_REDUCE_METHOD] = fn;
}

/**
 * Reduces xs with f starting from init if xs is {@link reduceReducible Reducible}. Throws error otherwise.
 *
 * @memberof reduce
 * @param {function} f a reduceing function
 * @param {any} init an initial value for the reduction
 * @param {reduce.Reducible} xs anything that can self reduce
 * @returns {any} the result of the reduction
 *
 */
function reduce(f, init, xs) {
    if (can_self_reduce(xs)) {
        return reduce_method(xs).call(xs, f, init);
    }
    if(isReduced(init)){
      return unreduced(init);
    }
    for(let x of xs){
      init = f(init, x);
      if(isReduced(init)){
        return unreduced(init);
      }
    }
    return init;
}
    
reduce.unreduced = unreduced;
reduce.reduced = reduced;
reduce.reduce = reduce;
reduce.isReduced = isReduced;
reduce.addSelfReduceMethod = addSelfReduceMethod;

module.exports = reduce;
