const filter = require("./filter.js");



/**
 * Passes each item allow the reduction base on a given probabilty
 *
 * @memberof transducers
 *
 * @param {number} prob a probability number between 0 and 1
 *
 * @returns {Transducer} a transducer
 */
function randomSample(prob) {
    return filter(function(_) {
        return Math.random() < prob;
    });
}


module.exports = randomSample;
