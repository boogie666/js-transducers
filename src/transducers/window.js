const { unreduced } = require("../reduce");

/**
 * returns a slideing window if given size into the reduction
 * @memberof transducers
 * @example
 * 
 *  into([], window(2), [1,2,3,4]); // returns [[1,2], [2, 3], [3, 4], [4]]
 *
 * @param {number} size the size of the window
 * @returns {Transducer} a transducer
 */
function window(size){
    return function(rf){
        var window = [];
        return function(result, item){
            switch(arguments.length){
            case 0:
                return rf();
            case 1:
                let result_window = window.concat();
                window = [];
                return rf(unreduced(rf(result, result_window)));
            default:
                window.push(item);
                if(window.length === size){
                    let result_window = window.concat();
                    window.shift();
                    return rf(result, result_window);
                }
                return result;
            }
        };
    };
}


module.exports = window;
