const filter = require("./filter.js");


/**
 * The opositive of {@link transducersfilter filter}. Removes all items for which the predicate function returns true.
 *
 * @memberof transducers
 * @param {function} pred a preducate function
 * @returns {Transducer} a transducer
 */
function remove(pred) {
    return filter(function(item) {
        return !pred(item);
    });
}

module.exports = remove;
