const { unreduced, isReduced } = require("../reduce");


const NOTHING =  "@@com.boogie666.tranducers/nothing_" + Math.random() + "@@";

/**
 * Paritions items in reductions based on value return by given function
 *
 * @memberof transducers
 * @example 
 * function getGroupName(x){ return x.groupName; }
 *
 * into([], partitionBy(getGroupName), [{ groupName: "A", id: 1}, {groupName: "A", id: 2}, {groupName: "B", id: 3}, {groupName: "A", id:4}]);
 * // returns [ [{ groupName: "A", id: 1}, {groupName: "A", id: 2}], [{groupName: "B", id: 3}],  [{groupName: "A", id:4}] ]
 *
 * @param {function} fn the grouping function
 * @returns {Transducer} a transducer
 *
 */
function partitionBy(fn){
    return function(rf){
        let value = NOTHING;
        let partitions = [];
        return function(result, item){
            switch(arguments.length){
            case 0: return rf();
            case 1:
                if(partitions.length > 0){
                    result = unreduced(rf(result, partitions));
                    partitions = [];
                }
                return rf(result);
            default:
                let val = fn(item);
                let pval = value;
                value = val;
                if(pval === NOTHING || pval === val){
                    partitions.push(item);
                    return result;
                }else{
                    result = rf(result, partitions);
                    if(!isReduced(result)){
                        partitions = [item];
                    }else{
                        partitions = [];
                    }
                    return result;
                }
            }
        };
    };
}


module.exports = partitionBy;
