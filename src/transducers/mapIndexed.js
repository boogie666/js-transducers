/**
 * Same as {@link transducers/map} but the function f gets as a first argument the index
 * 
 * @memberof transducers
 * @param {function} f a function of two arguments to be applied to the index of the current item and the current item
 * @returns {Transducer} a transducer
 */
function mapIndexed(f) {
    return function(rf) {
        let index = 0;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                return rf(result, f(index++, item));
            }
        };
    };
}


module.exports = mapIndexed;
