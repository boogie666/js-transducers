const filter_indexed = require("./filterIndexed.js");

/**
 * Same as {@link transducersremove remove}, but the pred function is passed the index as well
 *
 * @memberof transducers
 * @param {function} pred a function of two arguments to be applied to the index of the current item and the current item
 * @returns {Transducer} a transducer
 */
function removeIndexed(pred) {
    return filter_indexed(function(idx, item) {
        return !pred(idx, item);
    });
}


module.exports = removeIndexed;
