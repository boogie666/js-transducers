/**
 *
 * All transducers can be required either toghether or separatly
 * @example
 * const xf = require("js-transducers/transducers"); //all of them
 *
 * const map = require("js-transducers/transducers/map"); //just one of them
 * 
 * 
 *
 * @namespace transducers
 */

module.exports = {
    map: require("./map"),
    mapTo: require("./mapTo"),
    filter: require("./filter"),
    keep: require("./keep"),
    drop: require("./drop"),
    take: require("./take"),
    cat: require("./cat"),
    mapcat: require("./mapcat"),
    mapIndexed: require("./mapIndexed"),
    keepIndexed: require("./keepIndexed"),
    filterIndexed: require("./filterIndexed"),
    partitionAll: require("./partitionAll"),
    partitionBy: require("./partitionBy"),
    takeWhile: require("./takeWhile"),
    dropWhile: require("./dropWhile"),
    randomSample: require("./randomSample"),
    dedupe: require("./dedupe"),
    distinct: require("./distinct"),
    interpose: require("./interpose"),
    remove: require("./remove"),
    removeIndexed: require("./removeIndexed"),
    takeNth: require("./takeNth"),
    window: require("./window"),
    transduce: require("./transduce")
};
