const { reduced } = require("../reduce");


/**
 * The opposite of {@link transducersdrop drop}. Take only uses the first n items in the reduction and stops after.
 *
 * @memberof transducers
 * @example 
 * 
 * into([], take(2), [1,2,3,4]); // returns [1,2]
 *
 * @param {number} n the number of items to use in reduction
 * @returns {Transducer} a transducer
 */
function take(n) {
    return function(xf) {
        var count = 0;
        return function(acc, item) {
            switch (arguments.length) {
            case 0:
                return xf();
            case 1:
                return xf(acc);
            default:
                if (count === n) {
                    return reduced(acc);
                }
                count++;
                return xf(acc, item);
            }
        };
    };
}

module.exports = take;
