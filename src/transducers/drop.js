
/**
 * Skips the first n items in a reduction
 * 
 * @memberof transducers
 * @example 
 * into([], drop(3), [1,2,3,4,5,6]); // returns [4,5,6]
 *
 * @param {number} n number of items to skip
 * @returns {Transducer} a transducer
 */
function drop(n) {
    return function(xf) {
        var count = n;
        return function(acc, item) {
            switch (arguments.length) {
            case 0:
                return xf();
            case 1:
                return xf(acc);
            default:
                if (count > 0) {
                    count--;
                    return acc;
                }
                return xf(acc, item);
            }
        };
    };
}


module.exports = drop;
