const { reduced } = require("../reduce");


/**
 * Uses items in reduction while the predicate returns true.
 * 
 * @memberof transducers
 * @example 
 *  into([], takeWhile(isEven), [2,4,5,6,7,8]); // returns [2,4]
 *
 * @param {function} pred a predicate function
 * @returns {Transducer} a transducer
 */
function takeWhile(pred) {
    return function(rf) {
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                if (!pred(item)) {
                    return reduced(result);
                } else {
                    return rf(result, item);
                }
            }
        };
    };
}

module.exports = takeWhile;
