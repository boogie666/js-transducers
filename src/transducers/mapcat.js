const { comp } = require("../util.js");
const map = require("./map.js");
const cat = require("./cat.js");

/**
 * Same the composition of {@link transducersmap map} and {@link transducerscat cat}
 *
 * @memberof transducers
 * @param {function} f the function to be applied to each item. Must return a reducible value
 * @returns {Transducer} a transducer
 */
function mapcat(f) {
    return comp(map(f), cat);
};


module.exports = mapcat;
