const { comp } = require("../util.js");
const mapIndexed = require("./mapIndexed.js");
const filter = require("./filter.js");


/**
 * Same as {@link transducers/keep} but the function f gets as a first argument the index
 * 
 * @memberof transducers
 * @param {function} pred a function of two arguments to be applied to the index of the current item and the current item
 * @returns {Transducer} a transducer
 */
function keepIndexed(pred) {
    return comp(mapIndexed(pred), filter(function(item) {
        return item != null;
    }));
}

module.exports = keepIndexed;
