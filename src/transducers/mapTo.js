const map = require("./map.js");

/**
 * returns a constant given value for each step, ignoring the input
 *
 * @memberof transducers
 * @example into([], mapTo(5), [1,2,3]); // returns [5,5,5]
 * @param {any} constant the value that will be used
 * @returns {Transducer} a transducer
 */
function mapTo(constant){
    return map(function(_) { return constant; });
};


module.exports = mapTo;
