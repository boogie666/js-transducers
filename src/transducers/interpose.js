const { isReduced } = require("../reduce");

/**
 * Injects given value inbetween each value if the reduction.
 *
 * @memberof transducers
 * @example 
 * into([], interpose("X"), [1,2,3]); // returns [1, "X", 2, "X", 3]
 *
 * @param {any} separator the value to inject
 * @returns {Transducer} a transducer
 */
function interpose(separator) {
    return function(rf) {
        let started = false;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                if (started) {
                    result = rf(result, separator);
                    if (isReduced(result)) {
                        return result;
                    }
                    return rf(result, item);
                }
                started = true;
                return rf(result, item);
            }
        };
    };
}


module.exports = interpose;
