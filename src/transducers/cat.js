const {
    reduce,
    isReduced,
    reduced
} = require("../reduce");



function preserving_reduced(rf) {
    return function(a, b) {
        var result = rf(a, b);
        if (isReduced(result)) {
            return reduced(result);
        }
        return result;
    };
}

/**
 * Concatenate the contents of each input into the reduction.
 * Input must be a reducible value.
 * cat is a transducer.
 *
 * @memberof transducers
 * @example into([], cat, [[1,2], [3,4], [5,6]]); // returns [1,2,3,4,5,6]
 *
 * @param {function} rf the reducing function
 * @returns {function} reducing function
 */
function cat(rf) {
    const rrf = preserving_reduced(rf);
    return function(acc, item) {
        switch (arguments.length) {
        case 0:
            return rf();
        case 1:
            return rf(acc);
        default:
            return reduce(rrf, acc, item);
        }
    };
}



module.exports = cat;
