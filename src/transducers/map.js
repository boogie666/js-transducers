/**
 * Applies function f on each step.
 *
 * @memberof transducers
 * @example 
 *  into([], map(x => x + 1), [1,2,3]); // returns [2,3,4]
 * @param {function} f the function to be applied to each step
 * @returns {Transducer} a transducer
 */
function map(f) {
    return function(xf) {
        return function(acc, item) {
            switch (arguments.length) {
            case 0:
                return xf();
            case 1:
                return xf(acc);
            default:
                return xf(acc, f(item));
            }
        };
    };
}

module.exports = map;
