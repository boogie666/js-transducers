
/**
 * Will only use every nth item in the reduction process. Zero based.
 *
 * @memberof transducers
 * @example 
 *  into([], takeNth(2), [1,2,3,4,5,6]); // returns [3,6]
 *
 * @param {number} n the index step of the reduction (i.e. take every 5th number)
 * @returns {Transducer} a transducer
 */
function takeNth(n) {
    return function(rf) {
        let count = -1;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                count++;
                if (count % n === 0) {
                    return rf(result, item);
                }
                return result;
            }
        };
    };
}


module.exports = takeNth;
