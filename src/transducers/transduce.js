const {
    reduce
} = require("../reduce");


function transduce_4(xform, reducer, init, xs) {
    const xf = xform(reducer);
    return xf(reduce(xf, init, xs));
}

function transduce_3(xform, reducer, xs) {
    const xf = xform(reducer);
    const init = xf();
    return xf(reduce(xf, init, xs));
}


/**
 * Executes a transduction with given transducer and reductions function
 *
 * @memberof transducers
 * @example
 *
 * const add = compleating((a,b) => a + b);
 *
 * transduce(map(inc), add, 0, [1,2,3]); // returns 8;
 *
 *
 * @param {Transducer} xform the transducer to be used
 * @param {ReducingFunction} reducer a reducting function, used as the final step in the transduction process
 * @param {any} [init=xform(reducer)()] optional initial value, it omitted the 0 arrity version of the process is called
 * @param {Reducible} xs any reducible values that the reducer knows how to reduce through
 *
 * @return {any} the result of the transduction
 *
 */
function transduce(xform, reducer, init, xs) {
    switch (arguments.length) {
        case 3:
            return transduce_3(xform, reducer, init);
        default:
            return transduce_4(xform, reducer, init, xs);
    }
}

module.exports = transduce;
