const { tripleEq } = require("../util.js");

/**
 * Only allows distinct values into the reduction.
 *
 * @memberof transducers
 *
 * @example into([], distinct(), [1,1,2,3,2,1]); // returns [1,2,3]
 *
 * @param {function} [eqFn=tripleEq] a function that is used to determine if two values are equal. Default is the === function.
 * @returns {Transducer} a transducer
 */
function distinct(eqFn) {
    eqFn = eqFn || tripleEq;
    return function(rf) {
        let seen_so_far = [];
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                seen_so_far = [];
                return rf(result);

            default:
                if (!seen_so_far.find(i => eqFn(i, item))) {
                    seen_so_far.push(item);
                    result = rf(result, item);
                }
                return result;
            }
        };
    };
}


module.exports = distinct;
