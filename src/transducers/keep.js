const { comp } = require("../util.js");
const map = require("./map.js");
const filter = require("./filter.js");


/**
 * Applies given function f to each element in reduction and only keeps the non-null ones.
 *
 * @memberof transducers
 * @example 
 * function getName(x){ return x.name; }
 * 
 * into([], keep(getName), [{name : "John"}, {name: "Paul"}, { name: "George"}, { tag: "The other guy"}]); 
 * // returns ["John", "Paul", "George"]
 *
 * // while map on the same thing
 * into([], map(getName), ...); // returns ["John", "Paul", "George", undefined]
 *
 * @param {function} f the functio to be applied to each item
 * @returns {Transducer} a transducer
 *
 */
function keep(f) {
    return comp(map(f), filter(function(item) {
        return item != null;
    }));
}

module.exports = keep;
