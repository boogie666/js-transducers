const { tripleEq } = require("../util.js");


const NOTHING =  "@@com.boogie666.tranducers/nothing_" + Math.random() + "@@";

/**
 * Removes consecutive duplicate values.
 * @memberof transducers
 * @example into([], dedupe(), [1,1,2,2,3,1]); // returns [1,2,3,1]
 * @param {function} [eqFn=tripleEq] the function to be used to determine if two values are equal
 * @returns {Transducer} a transducer
 */
function dedupe(eqFn) {
    eqFn = eqFn || tripleEq;
    return function(rf) {
        let previous_value = NOTHING;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                if (previous_value === NOTHING || !eqFn(previous_value, item)) {
                    result = rf(result, item);
                }
                previous_value = item;
                return result;
            }
        };
    };
}

module.exports = dedupe;
