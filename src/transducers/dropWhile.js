
/**
 * Skips items in the reduction while the predicate returns true.
 *
 * @memberof transducers
 * @example 
 * into([], dropWhile(isEven), [2,4,5,6,7,8]); // returns [5,6,7,8]
 *
 * @param {function} pred a predicate function
 * @returns {Transducer} a transducer
 * 
 */
function drop_while(pred) {
    return function(rf) {
        let done_droping = false;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                if (done_droping || !pred(item)) {
                    done_droping = true;
                    return rf(result, item);
                }
                return result;
            }
        };
    };
}


module.exports = drop_while;
