const { isReduced, unreduced } = require("../reduce");


/**
 * Splits items in reduction into several {@link array}s of given size
 * @memberof transducers
 * @example
 *
 * into([], paritionAll(2), [1,2,3,4]); // returns [[1,2], [3,4]]
 *
 * @param {number} size the size of each parition
 * @returns {Transducer} a transducer
 */
function partitionAll(size) {
    return function(rf) {
        let partitions = [];
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                if (partitions.length > 0) {
                    result = unreduced(rf(result, partitions));
                    partitions = [];
                }
                return rf(result);
            default:
                partitions.push(item);
                if (partitions.length === size) {
                    result = rf(result, partitions);
                    partitions = [];
                }
                return result;
            }
        };
    };
}


module.exports = partitionAll;
