

/**
 * Same {@link filter} but the pred function is passed the index as the first param
 * 
 * @memberof transducers
 * @param {function} pred a predicate function that take two args, the index and the value
 * @returns {Transducer} a transducer
 */
function filterIndexed(pred) {
    return function(rf) {
        let index = 0;
        return function(result, item) {
            switch (arguments.length) {
            case 0:
                return rf();
            case 1:
                return rf(result);
            default:
                if (pred(index++, item)) {
                    result = rf(result, item);
                }
                return result;
            }
        };
    };
}


module.exports = filterIndexed;
