
/**
 * Removes values from reduction if they don't match the predicate
 * 
 * @memberof transducers
 * @example 
 * into([], filter(isEven), [1,2,3,4,5,6]); // returns [2,4,6]
 *
 * @param {function} pred a predicate function
 * @returns {Transducer} a transducer
 */
function filter(pred) {
    return function(xf) {
        return function(acc, item) {
            switch (arguments.length) {
            case 0:
                return xf();
            case 1:
                return xf(acc);
            default:
                if (pred(item)) {
                    return xf(acc, item);
                }
                return acc;
            }
        };
    };
}

module.exports = filter;
