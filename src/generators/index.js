let { isReduced, unreduced } = require("../reduce");


function seq_rf(r, i){
  if(arguments.length === 0)
    return [];

  if(arguments.length === 2)
    r.push(i);

  return r;
}

function* sequance(xf, coll){
  let rf = xf(seq_rf);
  
  let acc = rf();
  for(var i = 0; i < coll.length; i++){
    let result = rf(acc, coll[i]);
    if(isReduced(result)){
      rf(unreduced(result));
      yield* acc;
      return;
    }
    yield* acc;
    acc.length = 0;
  }

  rf(acc);
  yield *acc;
}

module.exports = sequance;
