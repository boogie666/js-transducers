function extend(a, b) {
    Object.keys(a).forEach(function(key) {
        b[key] = a[key];
    });
}

const packages = {};
extend(require("./transducers"), packages);
module.exports = packages;

