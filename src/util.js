function triple_eq(a, b) {
    return a === b;
}

function comp(/*...fns*/) {
    var fns = arguments;
    return function(x) {
        for (var i = fns.length - 1; i >= 0; i--) {
            x = fns[i](x);
        }
        return x;
    };
}

function completing(fn) {
    return function(a, b) {
        switch (arguments.length) {
            case 0:
                return fn();
            case 1:
                return a;
            default:
                return fn(a, b);
        }
    };
}


module.exports = {
    tripleEq: triple_eq,
    comp: comp,
    completing: completing
};
