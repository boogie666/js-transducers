require("./array");

const {
    inherits
} = require("util");
const {
    Transform
} = require('stream');
const {
    isReduced,
    unreduced
} = require('./reduce');

const {
    map,
    mapcat,
    take
} = require("./transducers");
const {
    comp
} = require("./util.js");


inherits(TransduceStream, Transform);
function TransduceStream(transducer, options){
    if(!options){
        options = {};
    }
    if(!options.hasOwnProperty("objectMode")){
        options.objectMode = true;
    }
    if(!(this instanceof TransduceStream)){
        return new TransduceStream(transducer, options);
    }
    Transform.call(this, options);

    this._transformXf = transducer(function(stream, item){
        switch(arguments.length){
        case 0: throw new Error("Can't create stream from nothing");
        case 1: return stream;
        default:
            if(!stream._destroyed){
                stream.push(item);
            }
            return stream;
        }
    });
    return this;
}

TransduceStream.prototype._transform = function(chunk, enc, cb){
    if(!this._destroyed){
        var stream = this._transformXf(this, chunk);
        if(stream && isReduced(stream)){
            this._transformXf(this);
            this.destroy();
        }
    }
    cb();
};

TransduceStream.prototype._flush = function(cb){
    if(!this._destroyed){
        this._transformXf(this);
        this.destroy();
    }
    cb();
};

TransduceStream.prototype.destroy = function(err){
    if(!this._destroyed){
        this._destroyed = true;

        var self = this;
        process.nextTick(function(){
            if(err) self.emit('error', err);
            self.end();
        });
    }
};


module.exports = TransduceStream;
