const assert = require("assert").strict;
const partitionAll = require("../src/transducers/partitionAll.js");

const process = partitionAll(3)(function(a,b){
    return b;
});

//use the first 3 values
assert.equal(process(null, 1), null);
assert.equal(process(null, 2), null);
assert.deepEqual(process(null, 3), [1,2,3]);

