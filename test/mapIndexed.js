const assert = require("assert").strict;
const mapIndexed = require("../src/transducers/mapIndexed.js");

function with_index(i, a){
    return [i, a];
}


const process = mapIndexed(with_index)(function(a,b){
    return b;
});

assert.deepEqual(process(null, 3), [0,3]);
assert.deepEqual(process(null, 2), [1,2]);
assert.deepEqual(process(null, 2), [2,2]);
