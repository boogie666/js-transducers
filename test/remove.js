const assert = require("assert").strict;
const remove = require("../src/transducers/remove.js");

function isEven(a){
    return a % 2 === 0;
}


const process = remove(isEven)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 1);
assert.equal(process(0, 2), 0);
