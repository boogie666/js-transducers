const assert = require("assert").strict;
const filterIndexed = require("../src/transducers/filterIndexed.js");

function first2Items(i, a){
    return i <= 2;
}

const process = filterIndexed(first2Items)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 1);
assert.equal(process(0, 2), 2);
assert.equal(process(0, 3), 3);
assert.equal(process(0, 4), 0);
assert.equal(process(0, 5), 0);
