
const assert = require("assert").strict;
const distinct = require("../src/transducers/distinct.js");



const process = distinct()(function(a,b){
    return a + b;
});

//multiple non-consecutive 1's and only the first one counts
assert.equal(process(0, 1), 1);
assert.equal(process(0, 1), 0);
assert.equal(process(0, 1), 0);

assert.equal(process(0, 2), 2);

assert.equal(process(0, 1), 0);

