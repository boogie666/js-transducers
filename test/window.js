const assert = require("assert").strict;
const window = require("../src/transducers/window.js");


const process = window(2)(function(a,b){
    return b;
});


assert.deepEqual(process(null, 1), null);
assert.deepEqual(process(null, 2), [1,2]);
assert.deepEqual(process(null, 3), [2,3]);
