const assert = require("assert").strict;
const { isReduced } = require("../src/reduce");
const dropWhile = require("../src/transducers/dropWhile.js");


function is_3(val){
    return val === 3;
}

const process = dropWhile(is_3)(function(a,b){
    return a + b;
});

//use the first 3 values
assert.equal(process(0, 3), 0);
assert.equal(process(0, 3), 0);
assert.equal(process(0, 3), 0);
assert.equal(process(0, 2), 2);
assert.equal(process(0, 3), 3);
assert.equal(process(0, 4), 4);


