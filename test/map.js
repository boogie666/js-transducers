const assert = require("assert").strict;
const map = require("../src/transducers/map.js");

function inc(a){
    return a + 1;
}


const process = map(inc)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 2, "Inc before add");
