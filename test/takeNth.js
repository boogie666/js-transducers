const assert = require("assert").strict;
const takeNth = require("../src/transducers/takeNth.js");

const process = takeNth(2)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 1);
assert.equal(process(0, 2), 0);
assert.equal(process(0, 3), 3);
assert.equal(process(0, 4), 0);
assert.equal(process(0, 5), 5);


