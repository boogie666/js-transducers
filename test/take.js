const assert = require("assert").strict;
const { isReduced } = require("../src/reduce");
const take = require("../src/transducers/take.js");



const process = take(3)(function(a,b){
    return a + b;
});

//use the first 3 values
assert.equal(process(0, 1), 1);
assert.equal(process(0, 1), 1);
assert.equal(process(0, 1), 1);

var result = process(0, 1);
assert(isReduced(result));
assert.equal(result.value, 0);

