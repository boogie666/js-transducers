const assert = require("assert").strict;
const transduce = require("../src/transducers/transduce.js");
const map = require("../src/transducers/map.js");


let init_call_count = 0;
let compleat_call_count = 0;

function array_push_transducer(result, item) {
    switch (arguments.length) {
        case 0:
            init_call_count++;
            return [];
        case 1:
            compleat_call_count++;
            return result;
        default:
            result.push(item);
            return result;
    }
}



assert.deepEqual(transduce(map(x => x), array_push_transducer, [], [1, 2, 3]), [1, 2, 3]);

assert.equal(init_call_count, 0);
assert.equal(compleat_call_count, 1);

init_call_count = 0;
compleat_call_count = 0;
assert.deepEqual(transduce(map(x => x), array_push_transducer, [1, 2, 3]), [1, 2, 3]);


assert.equal(init_call_count, 1);
assert.equal(compleat_call_count, 1);
