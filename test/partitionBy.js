const assert = require("assert").strict;

require("../src/array");
const into = require("../src/into");
const partitionBy = require("../src/transducers/partitionBy.js");

function is_even(a){
    return a % 2 === 0;
}

const process = partitionBy(is_even);


assert.deepEqual(into([], process, [1,3,2,4,5,7,8]),
                 [[1,3], [2,4], [5,7], [8]]);

