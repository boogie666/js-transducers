const assert = require("assert").strict;
const keepIndexed = require("../src/transducers/keepIndexed.js");

function with_index(i, a){
    if(a % 2 === 0){
        return [i, a];
    }
    return null;
}


const process = keepIndexed(with_index)(function(a,b){
    return b;
});

assert.deepEqual(process(null, 1), null);
assert.deepEqual(process(null, 2), [1, 2]);
assert.deepEqual(process(null, 3), null);
assert.deepEqual(process(null, 4), [3, 4]);
