require("../src/array");
const assert = require("assert").strict;
const cat = require("../src/transducers/cat.js");



const process = cat(function(a,b){
    return a + b;
});

assert.equal(process(0, [1]), 1);
assert.equal(process(0, [1,2,3]), 6);
