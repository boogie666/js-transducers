
const assert = require("assert").strict;
const { isReduced } = require("../src/reduce");
const takeWhile = require("../src/transducers/takeWhile.js");


function is_3(val){
    return val === 3;
}

const process = takeWhile(is_3)(function(a,b){
    return a + b;
});

//use the first 3 values
assert.equal(process(0, 3), 3);
assert.equal(process(0, 3), 3);
assert.equal(process(0, 3), 3);
const result = process(0, 4);
assert(isReduced(result));
assert.equal(result.value, 0);
