const assert = require("assert").strict;
const drop = require("../src/transducers/drop.js");



const process = drop(3)(function(a,b){
    return a + b;
});

//drops the first 3 values
assert.equal(process(0, 1), 0);
assert.equal(process(0, 1), 0);
assert.equal(process(0, 1), 0);
assert.equal(process(0, 1), 1);

