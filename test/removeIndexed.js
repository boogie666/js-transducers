const assert = require("assert").strict;
const removeIndexed = require("../src/transducers/removeIndexed.js");

function first2Items(i, a){
    return i <= 2;
}

const process = removeIndexed(first2Items)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 0);
assert.equal(process(0, 2), 0);
assert.equal(process(0, 3), 0);
assert.equal(process(0, 4), 4);
assert.equal(process(0, 5), 5);
