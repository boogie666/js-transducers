const assert = require("assert").strict;
require("../src/array");
const into = require("../src/into");
const interpose = require("../src/transducers/interpose.js");


const process = interpose("Hello");


assert.deepEqual(into([], process, [1,2,3,4]),
                 [1,"Hello",
                  2, "Hello",
                  3, "Hello", 4]);


