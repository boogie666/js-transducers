require("../src/array");
const assert = require("assert").strict;
const mapcat = require("../src/transducers/mapcat.js");


function inc(a){
    return [a, a + 1];
}

const process = mapcat(inc)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 3);
assert.equal(process(0, 2), 5);


