require("../src/array");

const tests = [
    "./cat.js",
    "./filterIndexed.js",
    "./keep.js",
    "./partitionAll.js",
    "./take.js",
    "./dedupe.js",
    "./filter.js",
    "./mapcat.js",
    "./partitionBy.js",
    "./takeNth.js",
    "./distinct.js",
    "./mapIndexed.js",
    "./takeWhile.js",
    "./drop.js",
    "./interpose.js",
    "./map.js",
    "./removeIndexed.js",
    "./transduce.js",
    "./dropWhile.js",
    "./keepIndexed.js",
    "./mapTo.js",
    "./remove.js",
    "./window.js",
    "./into.js",
    "./lazy_seq.js",
    "./package_test.js"
];


tests.forEach(function(module) {
    console.log("Running tests in", module);
    require(module);
});
