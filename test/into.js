const assert = require("assert").strict;
require("../src/array");
const into = require("../src/into");
const map = require("../src/transducers/map.js");


const process = map(x => x + 1);


assert.deepEqual(into([], process ,[1,2,3]), [2,3,4]);
assert.deepEqual(into([], [1,2,3]), [1,2,3]);
