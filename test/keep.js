const assert = require("assert").strict;
require("../src/array");
const into = require("../src/into");
const keep = require("../src/transducers/keep.js");

function inc(a){
    return a % 2 === 0 ? a : null;
}


const process = keep(inc);


assert.deepEqual(into([], process, [1,2,3,4]), [2,4]);
