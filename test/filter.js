const assert = require("assert").strict;
const filter = require("../src/transducers/filter.js");

function isEven(a){
    return a % 2 === 0;
}


const process = filter(isEven)(function(a,b){
    return a + b;
});

assert.equal(process(0, 1), 0);
assert.equal(process(0, 2), 2);
