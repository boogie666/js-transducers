require("../src/array");
const assert = require("assert").strict;
const sequance = require("../src/sequance.js");
const map = require("../src/transducers/map.js");
const into = require("../src/into");

var mutation = false;
const process = map(function(x){
    mutation = true;
    return x;
});


var lazy_seq = sequance(process, [1,2,3]);

assert.equal(mutation, false);

var result = into([], map(x => x), lazy_seq);

assert.deepEqual(result, [1,2,3]);
assert.equal(mutation, true);
