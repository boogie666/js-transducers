const assert = require("assert").strict;
const index = require("../src/index.js");

const packages = {
    map: require("../src/transducers/map"),
    mapTo: require("../src/transducers/mapTo"),
    filter: require("../src/transducers/filter"),
    keep: require("../src/transducers/keep"),
    drop: require("../src/transducers/drop"),
    take: require("../src/transducers/take"),
    cat: require("../src/transducers/cat"),
    mapcat: require("../src/transducers/mapcat"),
    mapIndexed: require("../src/transducers/mapIndexed"),
    keepIndexed: require("../src/transducers/keepIndexed"),
    filterIndexed: require("../src/transducers/filterIndexed"),
    partitionAll: require("../src/transducers/partitionAll"),
    partitionBy: require("../src/transducers/partitionBy"),
    takeWhile: require("../src/transducers/takeWhile"),
    dropWhile: require("../src/transducers/dropWhile"),
    randomSample: require("../src/transducers/randomSample"),
    dedupe: require("../src/transducers/dedupe"),
    distinct: require("../src/transducers/distinct"),
    interpose: require("../src/transducers/interpose"),
    remove: require("../src/transducers/remove"),
    removeIndexed: require("../src/transducers/removeIndexed"),
    takeNth: require("../src/transducers/takeNth"),
    window: require("../src/transducers/window"),
    transduce: require("../src/transducers/transduce")
};

assert.deepEqual(Object.keys(packages), Object.keys(index));

Object.keys(packages).forEach(function(package) {
    assert.equal(
        index[package], packages[package], "Package " + package + " not exported"
    );
});
