const assert = require("assert").strict;
const mapTo = require("../src/transducers/mapTo.js");

const process = mapTo("Hello")(function(a,b){
    return a + b;
});

//use the first 3 values
assert.equal(process("", 3), "Hello");
assert.equal(process("", []), "Hello");

