# xform

Transducers for javascript

## Docs 

https://boogie666.gitlab.io/js-transducers/#js-transducers


## Install

`npm install --save js-tranducers`

## Usage

```js
require("js-transducers/array"); // when using into on js arrays
require("js-transducers/immutable") // when using into on immutable.js collections
const into = require("js-transducers/into");

const xf = require("js-transducers");
const { comp } = require("js-transducers/util");

const process = comp(
  xf.map(x => x + 1),
  xf.filter(x => x % 2 === 0)
);

into([], process, [1, 2, 3]);

```

## util.comp is more or less useless

But it's needed internaly in order to not have any dependencies...

Basically any propper compose implementation will work just fine...

for example Rambda.js:

```js
const process = R.compose(
  xf.map(x => x + 1), 
  xf.filter(x => x % 2 === 0)
);
```

is identical to:


```js
const process = util.comp(
  xf.map(x => x + 1), 
  xf.filter(x => x % 2 === 0)
);
```

So feel free to use any compose function you are comfortable with.


lodash compose works in the exact same way.


```js
const process = _.compose(
  xf.map(x => x + 1), 
  xf.filter(x => x % 2 === 0)
);
```

any other implementation of compose will work in the exact same way, as long as it applies the functions in reverse order

such that if you compose these 3 functions:

```js
function a(x){
  console.log("a");
  return x;
}
function b(x){
  console.log("b");
  return x;
}

function c(x){
  console.log("c");
  return x;
}

```

the output of the console logs will be "c,b,a".

if the result is "a,b,c" then the transducers need to be used in reverse order.

So with Rambda.js' pipe function the equivalent is:

```js
const process = R.pipe(
  xf.filter(x => x % 2 === 0), // first the filter
  xf.map(x => x + 1) // then the map
);

```

## How the lib is built

Check out (if you know romanian) my talk about this lib and how to build it from scratch here:
 
https://www.youtube.com/watch?v=z9iuqCsOghc

## FAQ

What's with the `require("js-transducers/array")` or `require("js-transducers/immutable")`?

js-transducers used a controled from monkey patching in order to add a better version of 'reduce' in order to enable tranducers.

the `into` tranductions context needs to be have some caracteristics in order to be a propper transduction context.

see https://clojure.org/reference/transducers the 'Creating Transducers' section.

